# Assassin's Creed Valhalla 

### Summary
In Assassin's Creed Valhalla, become Eivor, a mighty Viking raider and lead your clan from the harsh shores of Norway to a new home amid the lush farmlands of ninth-century England.

![wall](images/wp6201098.jpg)

### Basic Info 
 * Game Save Location (**Empress**): `C:\Users\"User Name"\AppData\Roaming\Goldberg UplayEmu Saves\13504` <br>

 > You can just move the retail save files to empress save location in **appdata**, and go into the `ini` file in the main game folder of the empress crack called `uplay_r1_loader`  https://prnt.sc/rs93un and set the **userid** to your original uplay id which you can find is the name of the folder your saves are in in the retail. Either way works. Cheers guys.

 * How to apply *xbox controller files*:
     1. Go to `xbox_controller_files` directory and copy all files. Paste those files to your game directory. 
     2. Open `x360ce_x64.exe` and setup with your controller. And then apply. 
     3. Enjoy! 

### System Requirements

![system-config](images/specs.png)

### Current System Configuration 

CPU: AMD Ryzen 3100 4-Core Processor <br>
GPU: AMD Radeon[TM] RX 470 Graphics <br>
Driver: 27.20.1034.6 <br> 
Display: HP P191 (1366x768) <br>
RAM: 8GB DDR4 3400Mhz <br>
Game Size: 65 GB (Empress Crack) <br>

### Benchmark Result
Av. CPU Usage : **64%**  Watt: **36W**  Temp: **59c** <br>
Av. GPU Usage : **99%**  Watt: **82W**  Temp: **75c** <br>
Av. VRAM Usage : **3.5 GB** <br>
Av. RAM Usage : **7.7 GB**  <br>
Av. FPS : **58 FPS** <br>
Game Setting : **Custom (Very High)** <br>

### Graphics Setting 
> This recommendation is for based on **GTX 1050 Ti** or **AMD RX 470** graphics card only.

**Screen Settings**
| Items | Selected Options |
| ---- | ------ |
| Display Resolution | 1366x768 |
| VSync | ON *(you can turn it off if you want to)* |

**Graphics Settings** 

| Items | Selected Options |
| ---- | ------ |
| Graphics Quality | Custom |
| Adaptive Quality | OFF |
| Anti-Aliasing | Medium |
| World Details | Very High | 
| Clutter | Very High |
| Shadows | High |
| Voloumic Clouds | High |
| Water | High |
| Screen Space Reflection | On |
| Environment Texture | Medium |
| Character | High |
| Depth of field | High |
| Motion Blur | On | 



### Quora 

<hr>

**Documentation By** <br>
Farhan Sadik 

Source ~ <br>
https://news.ubisoft.com/en-us/article/4W08O9de72cVDMNIXJaM4l/assassins-creed-valhalla-pc-specs-revealed <br>
https://www.systemrequirementslab.com/cyri/requirements/assassins-creed-valhalla/19915 <br>
https://www.epicgames.com/store/en-US/p/assassins-creed-valhalla <br>
https://www.reddit.com/r/CrackSupport/comments/mc22ac/assassin_creed_valhalla_legit_save_game_to_emprees/ <br>
